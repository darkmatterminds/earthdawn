---Version 1.36----
Added a "vs Health" section to the defenses area to allow misc adjustments to wound threshold, unconsciousness, and death ratings.

Cleaned up strain damage clicky on defenses.

---Version 1.35----
Fixxed a bug with the willforce calculation to now include the statmod field

---Version 1.34----
added a floor of 0 to taken damage and wounds

---Version 1.33----
clienttracker now has a working end of turn button for players

---Version 1.32----
Found a bug in double click karma, fixxed.

---Version 1.25----
Added the elusive 8th tab for the notes seciton (well elusive for me anyhow :) )
	The notes section uses a cool feature suggested for the nWoD ruleset by Foen (https://www.fantasygrounds.com/forums/showthread.php?t=9238&page=2) (hopefully it's okay I used this)
	This way the GM can have hidden notes on the character's sheet, quite handy.

Fixxed up some code on the threads section of adventure_items.  
	- Now the GM will always see the info text for each subsection of a thread, but when sharing the sheet players only see what is known, matching the inventory functionality for threaded items.  
	- Dragging the item into the player's inventory maintains known/unknown states
	- on the shared sheet, the player cannot toggle the known checkbox, but can toggle between test and research knowledge.



---Version 1.23----

The Earthdawn ruleset was orignally developed by Joshua.  Thanks!

A lot of features were borrowed from the 4E ruleset and adapted for use within the Earthdawn ruleset.  Thanks JPG & Smiteworks!

Many bug fixes, solutions, etc, came from scouring the community forums, a big thanks to all there!

If I missed anything/anyone, I mean no disrespect, it only happened through ignorance.  Just in case, thanks!

I cannot trace exactly where all graphics in the ruleset came from at this time, so hopefully nothing is restricted, if it is, please let me know and I'll get them removed/replaced.  As far as I am aware, the ones included should be freely usable.

** note:  I use VIm as my editor, and heavily employ vim folds, this can cause some issues with some xml readers (notably IE, removing the modeline at the top of the file seems to clear this effect), but shows no issues with FG.  Hopefully the fold markers aren't to much of an eyesore for those not using VIm.

-- Changes --

   --Layout of the character sheet--
	 Frontpage
		Vitals
		Attributes
			Values and step only, remove temp/current
		Disciplines (List box)
			Disp, Circle, 1/2magic shortcut, karma max, min, cost, step
		Defense
		Armor
		Move 
		Initiative
		Health w/duability talent area
			Need blood magic, total damage area calc'd field
			durability modifies WT, uncon, and death (modifier fields on all for things like woodskin)
			recovery dice
		legend point summary
			calculates and populates legend spent field, with a hover showing remaining legend.
	
	 Talents
	 	Talents (supports drops of type reftalentfull)
	 		Remove notes
	 		In subwindow add a link box
	 		Calc: Rank + attr + modifier = step
	 
	 	Talent Knacks
	 		add link box
	
	 Skills
	 	Half-Magic
	 		remove
	 	Skills 	(supports drops of type refskillfull and reftalentfull)
	 		similar to talents
	 		removed disp and karma checkboxes
	 		shortcut
	 
	 	Languages
	 		checkbox for r/w and speak
	 		shortcut
	
	 Spells
		Spells: (supports drops of type refspellfull)
			Circle
			Name
			# Threads
			Weaving Diff
			Range
			Duration
			Notes/Full Shortcut
				above +
				casting diff
				effect
				effect step
				shortcut
		Matrices:	
			Name
			rank
			spell
			Damage taken
			Death rating
			mystic armor
			Full
			shortcut	
	
	 Inventory 
	 	Inventory (Supports drops of type item)
	 		--Widen--
	 		number/count
	 		name
	 		Weight
	 		Value
	 		carried checkbox
	 		calc'd weight
	 		calc'd value
	 		Full Desc
	 		Add shortcut
		  full card:
			Main
				count
				carried?
				notes
				value
				total value (value * count)
				weight
				total weight (weight * count if carried)
				description
			threads
				listitem
					subcard
						thread rank
						thread cost
						Test knowledge (toggle)
							test knowledge Known (gmonly)
							test knowledge desc (visable only if known, always visable to gm)
							effect (gmonly)
							effect desc (visable only if known, always visable to gm)
						Research knowledge (toggle)
							research knowledge Known (gmonly)
							research knowledge desc (visable only if known, always visable to gm)
							deed known(gmonly)
							deed desc (visable only if known, always visable to gm)
			other
				listitem
					property (weight, cost, step, armorvalue, etc)
					value (of property)
	 	Weight
			calculates and applies penalties on dex when encumbered.
	 	Currency
			added all known currencies (as rarely as most are actually used...)
	 	remove armor
	
	
	Combat Tab - add
		attacks
			name
			attack step
			damage step
			defense (physical/spell/social checkboxes)
			type (physical/mystic checkboxes)
			range
			fullcard
			notes
	
			fullcard
				name
				attack
				  attribute
				  skill/talent
				  other
				  attack step
				damage
				  attribute
				  talent
				  other
				  damage step
				defense
				damage type
				strain
				range
				ammo	
		defenses
			Active
			name
			defense
		  	  physical
			  spell
			  social
			Armor
			  physical
		 	  mystic
			init mod  
			strain
			fullcard
			notes
	
		modifiers card
		Karma - see known issues
		Defense
		Move 
		Initiative
		Health
		pic and tokenslot in upper right
	       
	 Thread Tab - add
	 	Threads
			Thread, Rank, tied to, Effect, shortcut
			fullcard
	
		Blood Magic
			item, effects, damage
			fullcard
	
	 Notes - remove
		Legend points - front page

   Other:  changes/modifiers to stats/skills/spells, etc all cascade to apply changes to relevant values
		- attribute name in skill/talents/combat should be the full name:  ie Strength, Perception, etc
		- if karma/strain is enabled for attack or damage step, it is automatically applied deducted for doubleclick rolling, not for drag rolling

   Known issues:  in rare cases, the karma flag on the attacks page can get out of sync..  Usually this is because of a talent that allowed karma use being removed while karma was still allowed on the action, to resolve, add a talent that allows karma use, then uncheck karma box, remove talent, close and reopen combat action

  -- added gm notes as formatted text to desktop --

  -- campaign items supports drops of type item --

  -- campaign personalities supports drops of type npc --

  -- GM combat tracker --
	drag and drop of npc copies stats 
	drag and drop of pc links back to character sheet for updates
	drop of numberdata onto damage box applies damage directly
	drop of numberdata onto physical armor applies armor, then damage
	drop of numberdata onto mystical armor applies MA, then damage
	if damage applied is above wound threshold a wound is automatically added (though no penalty is applied to rolls)
	targetting is enabled
	modified effect expiration so that "End N" is used for "End effect in N rounds" as that makes more sense for ED
	Initiative rolls supported, and sorted.  Drag and drop of initiative totals also support in case of special talents
	links to npc card

  -- PC combat/client tracker --
	Fairly basic, allows targetting, shows textual damage assements

  - step table calcs to 100

  - in general most (hopefully all) step boxes allow for double click/drag rolling



I think this covers most of the changes I have made, hopefully I haven't missed anything important, enjoy!

- Kirbol
