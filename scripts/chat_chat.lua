function applyModifierStackToRoll(draginfo)
	if ModifierStack.isEmpty() then
		--[[ do nothing ]]
	elseif draginfo.getNumberData() == 0 and draginfo.getDescription() == "" then
		draginfo.setDescription(ModifierStack.getDescription());
		draginfo.setNumberData(ModifierStack.getSum());
	else
		local originalnumber = draginfo.getNumberData();
		local numstr = tostring(originalnumber);
		if originalnumber > 0 then
			numstr = "+" .. originalnumber;
		end

		local moddesc = ModifierStack.getDescription(true);
		local desc = draginfo.getDescription();
		
		if numstr ~= "0" then
			desc = desc .. " " .. numstr;
		end
		if moddesc ~= "" then
			desc = desc .. " (" .. moddesc .. ")";
		end
		
		draginfo.setDescription(desc);
		draginfo.setNumberData(draginfo.getNumberData() + ModifierStack.getSum());
	end
	
	ModifierStack.reset();
end

function onDiceLanded(draginfo)
	if ChatManager.getDieRevealFlag() then
		draginfo.revealDice(true);
	end

	if draginfo.isType("dice") then
		applyModifierStackToRoll(draginfo);
		
		local dicetable = {};
		dicetable = draginfo.getDieList();
		
		if draginfo.getCustomData() ~= "bonus" then
			CampaignRegistry.dicelist = {};
			CampaignRegistry.dicedescription = draginfo.getDescription();
			CampaignRegistry.diemodifier = draginfo.getNumberData();
		end
		
		bonustable = {};
		local size = table.maxn(dicetable);
		for i= 1,size do
			if dicetable[i].result == tonumber(string.sub(dicetable[i].type,2)) then
				table.insert(bonustable, dicetable[i].type);
				print("inserted " .. dicetable[i].type);
			end
			table.insert(CampaignRegistry.dicelist, dicetable[i]);
		end
		if table.maxn(bonustable) > 0 then
			print("Throw dice here");
			throwDice("dice", bonustable, 0, "Bonus", "bonus");
			return true;
		end
		
		local regtable = CampaignRegistry.dicelist;
		local count = table.maxn(regtable);
		local total=0;
		for i= 1,count do
			total=total+regtable[i].result;
		end
		total=total+CampaignRegistry.diemodifier;
		
		local entry = {};
		entry.text = CampaignRegistry.dicedescription .. " [Total: " .. total .. "]";
		entry.font = "systemfont"
		entry.dice = CampaignRegistry.dicelist;
		entry.diemodifier = CampaignRegistry.diemodifier;
		if User.isHost() then
			entry.sender= GmIdentityManager.getCurrent();
			if ChatManager.getDieRevealFlag() then
				entry.dicesecret = false;
				deliverMessage(entry);
			else
				deliverMessage(entry);
			end
			
		else
			entry.sender = User.getIdentityLabel();
			deliverMessage(entry);
		end
		
		return true;		
	end
end

function onDrop(x, y, draginfo)
	if draginfo.getType() == "number" then
		applyModifierStackToRoll(draginfo);
	end
end

function moduleActivationRequested(module)
	local msg = {};
	msg.text = "Players have requested permission to load '" .. module .. "'";
	msg.font = "systemfont";
	msg.icon = "indicator_moduleloaded";
	addMessage(msg);
end

function moduleUnloadedReference(module)
	local msg = {};
	msg.text = "Could not open sheet with data from unloaded module '" .. module .. "'";
	msg.font = "systemfont";
	addMessage(msg);
end

function onInit()
	ChatManager.registerControl(self);
	
	if User.isHost() then
		Module.onActivationRequested = moduleActivationRequested;
	end

	Module.onUnloadedReference = moduleUnloadedReference;
end

function onClose()
	ChatManager.registerControl(nil);
end

function onReceiveMessage(msg)
	-- Special handling for client-host behind the scenes communication
	if ChatManager.processSpecialMessage(msg) then
		return true;
	end
	
	-- Otherwise, let FG know to do standard processing
	return false;
end

