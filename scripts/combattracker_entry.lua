--[[ vim: set tabstop=4 shiftwidth=4 foldmethod=marker commentstring=\-\-\[\[\%s\]\]: ]]
-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

targetingon = false;
activeon = false;
defensiveon = false;
spacingon = false;
effectson = false;

function onInit()--[[{{{]]
	-- Set the displays to what should be shown
	setTargetingVisible(false);
	setActiveVisible(false);
	setDefensiveVisible(false);
	setSpacingVisible(false);
	setEffectsVisible(false);

	-- Acquire token reference, if any
	linkToken();
	
	-- Set up the PC links
	if type.getValue() == "pc" then
		linkPCFields();
	end
	
	-- Update the displays
	updateDisplay();
	onWoundsChanged();
	
	-- Register the deletion menu item for the host
	registerMenuItem("Delete Item", "delete", 6);

	-- Track the effects list
	effects.getDatabaseNode().onChildUpdate = onEffectsChanged;
	effects.getDatabaseNode().onChildAdded = onEffectsChanged;
	onEffectsChanged();
	
	-- Track the targets list
	targets.getDatabaseNode().onChildUpdate = onTargetsChanged;
	targets.getDatabaseNode().onChildAdded = onTargetsChanged;
	onTargetsChanged();
	
	-- ENSURE ONE ENTRY FOR LAYOUT STABILITY
	if #(attacks.getWindows()) == 0 then
		attacks.createWindow();
	end
	
	-- SHOW THE TARGETING FEATURES, IF ENABLED
	if PremiumTargetingManager then
		activatetargeting.setVisible(true);
	end
end--[[}}}]]

function updateDisplay()--[[{{{]]
	local sFaction = friendfoe.getStringValue();

	if type.getValue() ~= "pc" then
		name.setFrame("textlinesmall", 0, 0, 0, 0);
	end

	if isActive() then
		name.setFont("ct_active");
		
		active_spacer_top.setVisible(true);
		active_spacer_bottom.setVisible(true);
		
		if sFaction == "friend" then
			setFrame("ctentrybox_friend_active");
		elseif sFaction == "neutral" then
			setFrame("ctentrybox_neutral_active");
		elseif sFaction == "foe" then
			setFrame("ctentrybox_foe_active");
		else
			setFrame("ctentrybox_active");
		end
	else
		name.setFont("ct_name");
		
		active_spacer_top.setVisible(false);
		active_spacer_bottom.setVisible(false);
		
		if sFaction == "friend" then
			setFrame("ctentrybox_friend");
		elseif sFaction == "neutral" then
			setFrame("ctentrybox_neutral");
		elseif sFaction == "foe" then
			setFrame("ctentrybox_foe");
		else
			setFrame("ctentrybox");
		end
	end
end--[[}}}]]

function linkToken()--[[{{{]]
	if tokenrefid and tokenrefnode then
		local imageinstance = token.populateFromImageNode(tokenrefnode.getValue(), tokenrefid.getValue());
		if imageinstance then
			token.acquireReference(imageinstance);
		end
	end
end--[[}}}]]

function onMenuSelection(selection)--[[{{{]]
	if selection == 6 then
		delete();
	end
end--[[}}}]]

function delete()--[[{{{]]
	-- Remember node name
	local sNode = getDatabaseNode().getNodeName();
	
	-- Clear any effects first, so that saves aren't triggered by nextActor
	effects.reset(false);
	
	-- Move to the next actor, if this CT entry is active
	if isActive() then
		windowlist.nextActor();
	end

	-- If this is an NPC with a token on the map, then remove the token also
	if type.getValue() == "npc" then
		token.deleteReference();
	end

	-- Delete the database node and close the window
	getDatabaseNode().delete();

	-- Update list information (global subsection toggles, targeting)
	windowlist.onVisibilityToggle();
	windowlist.onEntrySectionToggle();
	windowlist.deleteTarget(sNode);
	windowlist.rebuildClientTargeting();
end--[[}}}]]

function onTypeChanged()--[[{{{]]
	-- If a PC, then set up the links to the char sheet
	local sType = type.getValue();
	if sType == "pc" then
		linkPCFields();
	end

	-- If a NPC, then show the NPC display button; otherwise, hide it
	if sType == "npc" then
		show_npc.setVisible(true);
	else
		show_npc.setVisible(false);
	end
end--[[}}}]]

function onWoundsChanged()--[[{{{]]
	-- Based on the percent wounded, change the font color for the Wounds field
	if totaldamage.getValue() <= 0 then
		totaldamage.setFont("ct_healthy_number");
	elseif totaldamage.getValue() >= deathrating.getValue()  then
		totaldamage.setFont("ct_dead_number");
	elseif totaldamage.getValue() >= unconrating.getValue()  then
		totaldamage.setFont("ct_bloodied_number");
	elseif totaldamage.getValue() <= unconrating.getValue() / 2 then
		totaldamage.setFont("ct_ltwound_number");
	else 
		totaldamage.setFont("ct_ltwound_number");
	end

	-- Based on the percent wounded, set the Status text field
	if totaldamage.getValue() <= 0 then
		status.setValue("Healthy");
	elseif totaldamage.getValue() >= deathrating.getValue()  then
		status.setValue("Dead");
	elseif totaldamage.getValue() >= unconrating.getValue()  then
		status.setValue("Unconscious");
	elseif totaldamage.getValue() <= unconrating.getValue() / 2 then
		status.setValue("Light");
	else
		status.setValue("Damaged");
	end
	-- Update the token underlay to reflect wound status
	token.updateUnderlay();
end--[[}}}]]

function onSurgesChanged()--[[{{{]]
	-- Update the healing surges remaining field when healing surges max or healing surges used changes
	if type.getValue() == "pc" then
		--[[healsurgeremaining.setValue(healsurgesmax.getValue() - healsurgesused.getValue());]]
	end
end--[[}}}]]

function onFactionChanged()--[[{{{]]
	-- Update the entry frame
	updateDisplay();

	-- Update the token underlay to friend-or-foe status
	token.updateUnderlay();
end--[[}}}]]

function onEffectsChanged()--[[{{{]]
	-- SET THE EFFECTS CONTROL STRING
	local affectedby = EffectsManager.getEffectsString(getDatabaseNode());
	effects_str.setValue(affectedby);
	
	-- UPDATE VISIBILITY
	if affectedby == "" or effectson then
		effects_label.setVisible(false);
		effects_str.setVisible(false);
	else
		effects_label.setVisible(true);
		effects_str.setVisible(true);
	end
	setSpacerState();
end--[[}}}]]

function onTargetsChanged()--[[{{{]]
	-- VALIDATE (SINCE THIS FUNCTION CAN BE CALLED BEFORE FULLY INSTANTIATED)
	if not targets_str then
		return;
	end
	
	-- GET TARGET NAMES
	local aTargetNames = {};
	for keyTarget, winTarget in pairs(targets.getWindows()) do
		local sTargetName = NodeManager.get(DB.findNode(winTarget.noderef.getValue()), "name", "");
		if sTargetName == "" then
			sTargetName = "<Target>";
		end
		table.insert(aTargetNames, sTargetName);
	end

	-- SET THE TARGETS CONTROL STRING
	targets_str.setValue(table.concat(aTargetNames, ", "));
	
	-- UPDATE VISIBILITY
	if #aTargetNames == 0 or targetingon then
		targets_label.setVisible(false);
		targets_str.setVisible(false);
	else
		targets_label.setVisible(true);
		targets_str.setVisible(true);
	end
	setSpacerState();
end--[[}}}]]

function onClientTargetingUpdate()--[[{{{]]
	windowlist.rebuildClientTargeting();
end--[[}}}]]

function setSpacerState()--[[{{{]]
	if effects_label.isVisible() then
		if targets_label.isVisible() then
			spacer2.setAnchoredHeight(2);
		else
			spacer2.setAnchoredHeight(6);
		end
	else
		spacer2.setAnchoredHeight(0);
	end
end--[[}}}]]

function linkPCFields(src)--[[{{{]]
	local src = link.getTargetDatabaseNode();
	if src then
		name.setLink(NodeManager.createChild(src, "name", "string"), true);
		wounds.setLink(NodeManager.createChild(src, "health.wounds", "number"));
		wounds.setReadOnly(true);
		damagetaken.setLink(NodeManager.createChild(src, "health.damage", "number"));
		blooddamage.setLink(NodeManager.createChild(src, "health.bmdamage.base", "number"));
		--totaldamage.setLink(NodeManager.createChild(src, "totdamage", "number"));
		mysticarmor.setLink(NodeManager.createChild(src, "armor.mystic.total", "number"));
		mysticarmor.setReadOnly(true);
		physicalarmor.setLink(NodeManager.createChild(src, "armor.physical.total", "number"));
		physicalarmor.setReadOnly(true);
		init.setLink(NodeManager.createChild(src, "init.step.total", "number"), true);
		init.setReadOnly(true);

		physicaldef.setLink(NodeManager.createChild(src, "defense.physical.total", "number"), true);
		physicaldef.setReadOnly(true);
		spelldef.setLink(NodeManager.createChild(src, "defense.spell.total", "number"), true);
		spelldef.setReadOnly(true);
		socialdef.setLink(NodeManager.createChild(src, "defense.social.total", "number"), true);
		socialdef.setReadOnly(true);

		unconrating.setLink(NodeManager.createChild(src, "health.unconscious.total", "number"), true);
		unconrating.setReadOnly(true);
		deathrating.setLink(NodeManager.createChild(src, "health.death.total", "number"), true);
		deathrating.setReadOnly(true);
		woundthresh.setLink(NodeManager.createChild(src, "health.woundthreshold.total", "number"), true);
		woundthresh.setReadOnly(true);
		
		rectsts.setLink(NodeManager.createChild(src, "health.recovery.current.total", "number"), true);
		rectsts.setReadOnly(true);
		recstep.setLink(NodeManager.createChild(src, "recovery.step.total", "number"), true);
		recstep.setReadOnly(true);


--[[
		numattacks.setReadOnly(true);
		numattackslabel.setReadOnly(true);
		attackstep.setReadOnly(true);
		attacksteplabel.setReadOnly(true);
		damagestep.setReadOnly(true);
		damagesteplabel.setReadOnly(true);
		numspells.setReadOnly(true);
		numspellslabel.setReadOnly(true);
		spellstep.setReadOnly(true);
		spellsteplabel.setReadOnly(true);
		effectstep.setReadOnly(true);
		effectsteplabel.setReadOnly(true);
]]
		
		combatmv.setLink(NodeManager.createChild(src, "move.combat", "number"), true);
		fullmv.setLink(NodeManager.createChild(src, "move.full", "number"), true);

	end
end--[[}}}]]

--
-- SECTION VISIBILITY FUNCTIONS
--

function setTargetingVisible(v)--[[{{{]]
	if activatetargeting.getValue() then
		v = true;
	end
	if type.getValue() ~= "pc" and active.getState() then
		v = true;
	end
	
	targetingon = v;
	targetingicon.setVisible(v);
	
	targeting_add_button.setVisible(v);
	targeting_clear_button.setVisible(v);
	targets.setVisible(v);
	
	onTargetsChanged();
end--[[}}}]]

function setActiveVisible(v)--[[{{{]]
	if activateactive.getValue() then
		v = true;
	end
	if type.getValue() ~= "pc" and active.getState() then
		v = true;
	end
	
	activeon = v;
	activeicon.setVisible(v);

	--immediate_check.setVisible(v);
	effectstep.setVisible(v);
	effectsteplabel.setVisible(v);
	spellstep.setVisible(v);
	spellsteplabel.setVisible(v);
	numspells.setVisible(v);
	numspellslabel.setVisible(v);
	damagestep.setVisible(v);
	damagesteplabel.setVisible(v);
	attackstep.setVisible(v);
	attacksteplabel.setVisible(v);
	numattacks.setVisible(v);
	numattackslabel.setVisible(v);
	init.setVisible(v);
	initlabel.setVisible(v);
	attacks.setVisible(v);
	--atklabel.setVisible(v);
end--[[}}}]]

function setDefensiveVisible(v)--[[{{{]]
	if activatedefensive.getValue() then
		v = true;
	end
	
	defensiveon = v;
	defensiveicon.setVisible(v);

	physicaldef.setVisible(v);
	physicaldeflabel.setVisible(v);
	spelldef.setVisible(v);
	spelldeflabel.setVisible(v);
	socialdef.setVisible(v);
	socialdeflabel.setVisible(v);

	deathrating.setVisible(v);
	deathratinglabel.setVisible(v);
	woundthresh.setVisible(v);
	woundthreshlabel.setVisible(v);
	unconrating.setVisible(v);
	unconratinglabel.setVisible(v);

	rectsts.setVisible(v);
	rectstslabel.setVisible(v);
	recstep.setVisible(v);
	recsteplabel.setVisible(v);

	specialdef.setVisible(v);
	specialdeflabel.setVisible(v);
end--[[}}}]]
	
function setSpacingVisible(v)--[[{{{]]
	if activatespacing.getValue() then
		v = true;
	end

	spacingon = v;
	spacingicon.setVisible(v);
	spacingframe.setVisible(v);
	
	combatmv.setVisible(v);
	combatmvlabel.setVisible(v);
	fullmv.setVisible(v);
	fullmvlabel.setVisible(v);
	space.setVisible(v);
	spacelabel.setVisible(v);
	reach.setVisible(v);
	reachlabel.setVisible(v);
end--[[}}}]]

function setEffectsVisible(v)--[[{{{]]
	if activateeffects.getValue() then
		v = true;
	end
	
	effectson = v;
	effecticon.setVisible(v);
	
	effects.setVisible(v);
	if v then
		effects.checkForEmpty();
	end
	
	onEffectsChanged();
end--[[}}}]]

-- Activity state

function isActive()--[[{{{]]
	return active.getState();
end--[[}}}]]

function setActive(state)--[[{{{]]
	-- Set the active indicator
	active.setState(state);
	
	-- Visible changes
	updateDisplay();
	
	-- Notifications 
	if state then
		-- Turn notification
		local msg = {font = "narratorfont", icon = "indicator_flag"};
		msg.text = "[TURN] " .. name.getValue();

		if type.getValue() == "pc" then
			-- Player Turn notification
			ChatManager.deliverMessage(msg);
			
			-- Ring bell also, if option enabled
			if OptionsManager.isOption("RING", "on") then
				local usernode = link.getTargetDatabaseNode();
				if usernode then
					local ownerid = User.getIdentityOwner(usernode.getName());
					if ownerid then
						User.ringBell(ownerid);
					end
				end
			end
		else
			-- DM Turn notification
			if show_npc.getState() then
				ChatManager.deliverMessage(msg);
			else
				msg.text = "[GM] " .. msg.text;
				ChatManager.addMessage(msg);
			end
		end
	end
end--[[}}}]]

-- Client Visibility

function isClientVisible()--[[{{{]]
	if type.getValue() == "pc" then
		return true;
	end
	if show_npc.getState() then
		return true;
	end
	return false;
end--[[}}}]]
